package co.com.sura.ciudadela;

import co.com.sura.ciudadela.application.command.CreateConstructionRequestCommand;
import co.com.sura.ciudadela.application.commandbus.CommandBus;
import co.com.sura.ciudadela.model.event.ConstructionRequestDate;
import co.com.sura.ciudadela.model.event.ConstructionRequestSchedule;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class CiudadelaApplication implements CommandLineRunner {


	@Autowired
	private CommandBus commandBus;

	@Override
	public void run(String... args) throws Exception {
		System.out.println("Invocando commandBus");

		ConstructionRequestSchedule constructionRequestSchedule = ConstructionRequestSchedule.valueOf(ConstructionRequestDate.valueOf("2021-09-03T10:15:30"), ConstructionRequestDate.valueOf("2021-12-03T22:00:00"));
		CreateConstructionRequestCommand command = CreateConstructionRequestCommand.Builder.getInstance()
				.eventSchedule(constructionRequestSchedule)
				.build();
		commandBus.handle(command);
	}

	public static void main(String[] args) throws Exception {
		SpringApplication.run(CiudadelaApplication.class, args);
	}

}
