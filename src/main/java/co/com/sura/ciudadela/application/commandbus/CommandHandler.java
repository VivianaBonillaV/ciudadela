package co.com.sura.ciudadela.application.commandbus;

public interface CommandHandler <T extends CreateConstructionRequestCommand>{
    void handle(T command) throws Exception;
}
