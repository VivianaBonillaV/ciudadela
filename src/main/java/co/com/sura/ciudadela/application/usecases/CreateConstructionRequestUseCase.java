package co.com.sura.ciudadela.application.usecases;

import co.com.sura.ciudadela.model.domaineventbus.DomainEventBus;
import co.com.sura.ciudadela.model.event.*;
import org.springframework.stereotype.Component;

@Component
public class CreateConstructionRequestUseCase {

    private ConstructionRequestRepository constructionRequestRepository;
    private DomainEventBus domainEventBus;

    public CreateConstructionRequestUseCase(ConstructionRequestRepository constructionRequestRepository, DomainEventBus domainEventBus) {
        this.constructionRequestRepository = constructionRequestRepository;
        this.domainEventBus = domainEventBus;
    }

    public void handle(ConstructionRequestId id, ConstructionRequestType type, ConstructionRequestUbication ubication) throws Exception {
        ConstructionRequest constructionRequest = ConstructionRequest.create(id, type, ubication);
        constructionRequestRepository.persist(constructionRequest);

        domainEventBus.publish(constructionRequest.getDomainEvents());
    }
}
