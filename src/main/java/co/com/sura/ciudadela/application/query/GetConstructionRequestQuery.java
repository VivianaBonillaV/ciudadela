package co.com.sura.ciudadela.application.query;

import co.com.sura.ciudadela.application.querybus.Query;
import co.com.sura.ciudadela.model.event.ConstructionRequest;
import co.com.sura.ciudadela.model.event.ConstructionRequestId;

public class GetConstructionRequestQuery extends Query<ConstructionRequest> {
    private ConstructionRequestId eventId;

    public GetConstructionRequestQuery(ConstructionRequestId eventId) {
        this.eventId = eventId;
    }

    public ConstructionRequestId getEventId() {
        return eventId;
    }

    public static class Builder {

        private ConstructionRequestId eventId;

        public static GetConstructionRequestQuery.Builder getInstance() {
            return new GetConstructionRequestQuery.Builder();
        }

        public GetConstructionRequestQuery.Builder eventId(ConstructionRequestId constructionRequestId) {
            this.eventId = constructionRequestId;
            return this;
        }

        public GetConstructionRequestQuery build() {
            return new GetConstructionRequestQuery(eventId);
        }
    }
}
