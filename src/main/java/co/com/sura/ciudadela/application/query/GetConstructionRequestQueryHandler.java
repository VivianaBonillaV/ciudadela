package co.com.sura.ciudadela.application.query;

import co.com.sura.ciudadela.application.querybus.QueryHandler;
import co.com.sura.ciudadela.model.event.ConstructionRequest;
import co.com.sura.ciudadela.model.event.ConstructionRequestRepository;
import org.springframework.stereotype.Component;

@Component
public class GetConstructionRequestQueryHandler implements QueryHandler<ConstructionRequest, GetConstructionRequestQuery> {
    private ConstructionRequestRepository eventRepository;

    public GetConstructionRequestQueryHandler(ConstructionRequestRepository eventRepository) {
        this.eventRepository = eventRepository;
    }

    @Override
    public ConstructionRequest handle(GetConstructionRequestQuery query) throws Exception {
        return eventRepository.findById(query.getEventId());
    }
}
