package co.com.sura.ciudadela.application.usecases;

import co.com.sura.ciudadela.model.event.ConstructionRequestId;
import co.com.sura.ciudadela.model.event.ConstructionRequestRepository;
import co.com.sura.ciudadela.model.event.ConstructionRequest;
import org.springframework.stereotype.Component;

@Component
public class GetConstructionRequestUseCase {
    private ConstructionRequestRepository eventRepository;

    public GetConstructionRequestUseCase(ConstructionRequestRepository eventRepository) {
        this.eventRepository = eventRepository;
    }

    public ConstructionRequest handle(ConstructionRequestId id) {
        return eventRepository.findById(id);
    }
}
