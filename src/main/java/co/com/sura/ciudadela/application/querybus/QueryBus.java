package co.com.sura.ciudadela.application.querybus;

public interface QueryBus {
    <T> T handle(Query<T> query) throws Exception;
}
