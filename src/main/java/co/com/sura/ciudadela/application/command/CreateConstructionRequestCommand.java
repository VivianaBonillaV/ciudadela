package co.com.sura.ciudadela.application.command;

import co.com.sura.ciudadela.model.event.ConstructionRequestSchedule;
import co.com.sura.ciudadela.model.event.ConstructionRequestType;
import co.com.sura.ciudadela.model.event.ConstructionRequestUbication;

public class CreateConstructionRequestCommand extends co.com.sura.ciudadela.application.commandbus.CreateConstructionRequestCommand {
    private ConstructionRequestSchedule constructionRequestSchedule;
    private ConstructionRequestType eventType;
    private ConstructionRequestUbication ubication;

    private CreateConstructionRequestCommand(ConstructionRequestType eventType, ConstructionRequestUbication ubication) {
        this.eventType = eventType;
        this.ubication = ubication;
    }

    public ConstructionRequestSchedule getEventSchedule() {
        return constructionRequestSchedule;
    }
    public ConstructionRequestType getEventType(){
        return eventType;
    }
    public ConstructionRequestUbication getUbication(){
        return ubication;
    }

    public static class Builder {

        private ConstructionRequestSchedule constructionRequestSchedule;
        private ConstructionRequestType constructionRequestType;
        private ConstructionRequestUbication ubication;

        public static Builder getInstance() {
            return new Builder();
        }

        public Builder eventSchedule(ConstructionRequestSchedule constructionRequestSchedule) {
            this.constructionRequestSchedule = constructionRequestSchedule;
            return this;
        }
        public Builder eventInformation(ConstructionRequestType constructionRequestType, ConstructionRequestUbication ubication){
            this.constructionRequestType = constructionRequestType;
            this.ubication = ubication;
            return this;

        }

        public CreateConstructionRequestCommand build() {
            return new CreateConstructionRequestCommand(constructionRequestType,ubication);
        }
    }

}
