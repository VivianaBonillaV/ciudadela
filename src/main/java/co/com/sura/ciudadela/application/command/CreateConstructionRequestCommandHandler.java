package co.com.sura.ciudadela.application.command;

import co.com.sura.ciudadela.application.commandbus.CommandHandler;
import co.com.sura.ciudadela.application.usecases.CreateConstructionRequestUseCase;
import co.com.sura.ciudadela.model.event.ConstructionRequestId;
import co.com.sura.ciudadela.model.event.ConstructionRequestRepository;
import org.springframework.stereotype.Component;

@Component
public class CreateConstructionRequestCommandHandler implements CommandHandler<CreateConstructionRequestCommand> {
    private ConstructionRequestRepository eventRepository;
    private CreateConstructionRequestUseCase useCase;

    public CreateConstructionRequestCommandHandler(ConstructionRequestRepository eventRepository, CreateConstructionRequestUseCase useCase) {
        this.eventRepository = eventRepository;
        this.useCase = useCase;
    }

    @Override
    public void handle(CreateConstructionRequestCommand command) throws Exception {
        System.out.println("Creando evento y validando ");
        ConstructionRequestId id = eventRepository.getId();
        useCase.handle(id, command.getEventType(),command.getUbication());
    }

}
