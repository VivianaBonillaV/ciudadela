package co.com.sura.ciudadela.application.commandbus;

public interface CommandBus {
    void handle(CreateConstructionRequestCommand command) throws Exception;
}
