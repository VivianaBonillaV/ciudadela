package co.com.sura.ciudadela.model.event;

import co.com.sura.ciudadela.model.domaineventbus.DomainEvent;

public class ConstructionRequestCreatedDomainEvent extends DomainEvent {
    private ConstructionRequestId eventId;

    public ConstructionRequestCreatedDomainEvent(ConstructionRequestId eventId) {
        this.eventId = eventId;
    }

    public ConstructionRequestId getEventId() {
        return eventId;
    }
}
