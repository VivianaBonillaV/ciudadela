package co.com.sura.ciudadela.model.event;

import co.com.sura.ciudadela.model.event.exceptions.InvalidDate;

import java.time.LocalDateTime;
import java.time.format.DateTimeParseException;
import java.util.Objects;

public class ConstructionRequestDate {
    private final LocalDateTime datetime;

    private ConstructionRequestDate(final LocalDateTime datetime) throws InvalidDate {
        this.datetime = datetime;
    }

    public static ConstructionRequestDate valueOf(final String datetime) throws InvalidDate {
        return new ConstructionRequestDate(toDate(datetime));
    }

    public LocalDateTime geValue() {
        return datetime;
    }

    public boolean isAfter(final ConstructionRequestDate datetime) {
        return this.datetime.isAfter(datetime.getDateTime());
    }

    public boolean isFutureDate() {
        return datetime.isAfter(LocalDateTime.now());
    }



    private LocalDateTime getDateTime() {
        return datetime;
    }

    private static LocalDateTime toDate(final String date) throws InvalidDate {
        try {
            return LocalDateTime.parse(date);
        } catch (DateTimeParseException ex) {
            throw new InvalidDate();
        }
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null) return false;
        if (!(o instanceof ConstructionRequestDate)) return false;
        ConstructionRequestDate that = (ConstructionRequestDate) o;
        return Objects.equals(this.datetime, that.datetime);
    }

    @Override
    public int hashCode() {
        return Objects.hash(datetime);
    }
}
