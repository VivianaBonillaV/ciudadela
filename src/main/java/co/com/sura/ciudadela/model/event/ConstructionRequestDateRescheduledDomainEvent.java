package co.com.sura.ciudadela.model.event;

import co.com.sura.ciudadela.model.domaineventbus.DomainEvent;

public class ConstructionRequestDateRescheduledDomainEvent extends DomainEvent {
    private ConstructionRequestId eventId;

    public ConstructionRequestDateRescheduledDomainEvent(ConstructionRequestId eventId) {
        this.eventId = eventId;
    }

    public ConstructionRequestId getEventId() {
        return eventId;
    }
}
