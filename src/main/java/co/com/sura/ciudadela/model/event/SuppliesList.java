package co.com.sura.ciudadela.model.event;


import java.util.HashMap;
import java.util.Map;

public class SuppliesList {
    private Map<String, Integer> supplies;

    public SuppliesList() {
        this.supplies = new HashMap<>();;
        supplies.put("ce",1000);
        supplies.put("gr",1000);
        supplies.put("ar",1000);
        supplies.put("ma",1000);
        supplies.put("ad",1000);
    }

    public Map<String,Integer> getAll() {
        return supplies;
    }


    public void clear() {
        supplies.clear();
    }
}
