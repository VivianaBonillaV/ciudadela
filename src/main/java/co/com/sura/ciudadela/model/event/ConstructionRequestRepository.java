package co.com.sura.ciudadela.model.event;

public interface ConstructionRequestRepository {
    ConstructionRequestId getId();

    ConstructionRequest findById(ConstructionRequestId id);

    void persist(ConstructionRequest event);
}
