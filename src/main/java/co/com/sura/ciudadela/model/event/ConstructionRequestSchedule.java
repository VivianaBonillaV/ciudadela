package co.com.sura.ciudadela.model.event;

import co.com.sura.ciudadela.model.event.exceptions.EndDateIsBeforeStartDate;

import java.util.Objects;

public class ConstructionRequestSchedule {

    private ConstructionRequestDate startDate;
    private ConstructionRequestDate endDate;

    private ConstructionRequestSchedule(ConstructionRequestDate startDate, ConstructionRequestDate endDate) {
        this.startDate = startDate;
        this.endDate = endDate;
    }

    public ConstructionRequestDate getStartDate() {
        return startDate;
    }

    public ConstructionRequestDate getEndDate() {
        return endDate;
    }

    public boolean isFutureDate() {
        return startDate.isFutureDate();
    }

    public static ConstructionRequestSchedule valueOf(ConstructionRequestDate startDate, ConstructionRequestDate endDate) throws EndDateIsBeforeStartDate {
        validateStartBeforeEnd(startDate, endDate);
        return new ConstructionRequestSchedule(startDate, endDate);
    }

    private static void validateStartBeforeEnd(ConstructionRequestDate startDate, ConstructionRequestDate endDate) throws EndDateIsBeforeStartDate {
        if (startDate.isAfter(endDate)) {
            throw new EndDateIsBeforeStartDate();
        }
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null) return false;
        if (!(o instanceof ConstructionRequestSchedule)) return false;
        ConstructionRequestSchedule that = (ConstructionRequestSchedule) o;
        return Objects.equals(this.startDate, that.startDate)
                && Objects.equals(this.endDate, that.endDate);
    }

    @Override
    public int hashCode() {
        return Objects.hash(startDate, endDate);
    }
}
