package co.com.sura.ciudadela.model.event;

import java.math.BigInteger;
import java.util.Objects;

public class ConstructionRequestId {
    private BigInteger id;

    protected ConstructionRequestId() {
    }

    protected ConstructionRequestId(BigInteger id) {
        this.id = id;
    }

    public BigInteger getValue() {
        return id;
    }

    public static ConstructionRequestId valueOf(String id) {
        return new ConstructionRequestId(new BigInteger(id));
    }

    public static ConstructionRequestId valueOf(BigInteger id) {
        return new ConstructionRequestId(id);
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null) return false;
        if (!(o instanceof ConstructionRequestId)) return false;
        ConstructionRequestId that = (ConstructionRequestId) o;
        return Objects.equals(this.id, that.id);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id);
    }
}
