package co.com.sura.ciudadela.model.event;

import co.com.sura.ciudadela.model.domaineventbus.DomainEvent;

public class ConstructionRequestCancelledDomainEvent extends DomainEvent {

    private ConstructionRequestId eventId;

    public ConstructionRequestCancelledDomainEvent(ConstructionRequestId eventId) {
        this.eventId = eventId;
    }

    public ConstructionRequestId getEventId() {
        return eventId;
    }
}
