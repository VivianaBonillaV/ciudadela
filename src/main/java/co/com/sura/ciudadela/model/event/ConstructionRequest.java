package co.com.sura.ciudadela.model.event;

import co.com.sura.ciudadela.model.domaineventbus.DomainEventCollection;
import co.com.sura.ciudadela.model.event.exceptions.BusyLocation;
import co.com.sura.ciudadela.model.event.exceptions.EndDateIsBeforeStartDate;
import co.com.sura.ciudadela.model.event.exceptions.SuppliesOutStock;

import java.io.Serializable;

public class ConstructionRequest implements Serializable {
    private ConstructionRequestId id;
    private Status status;
    private ConstructionRequestSchedule schedule;
    private ConstructionRequestType type;
    private ConstructionRequestUbication ubication;

    private DomainEventCollection domainEvents;

    public enum Status {
        PENDIENTE, PROGRESO, FINALIZADO
    }

    public ConstructionRequest(ConstructionRequestId id, ConstructionRequestType type, ConstructionRequestUbication ubication) {
        this.id = id;
        this.status = Status.PENDIENTE;
        this.type = type;
        this.ubication = ubication;

        this.domainEvents = new DomainEventCollection();
    }

    public ConstructionRequestId getId() {
        return id;
    }

    public ConstructionRequest.Status getStatus() {
        return status;
    }

    public ConstructionRequestSchedule getSchedule() {
        return schedule;
    }
    public ConstructionRequestType getType() {
        return type;
    }
    public ConstructionRequestUbication getUbication() {
        return ubication;
    }

    public DomainEventCollection getDomainEvents() {
        return domainEvents;
    }

    public static ConstructionRequest create(ConstructionRequestId id, ConstructionRequestType type, ConstructionRequestUbication ubication) throws Exception {
        if (type!=null && !type.getSupplies(type)) {
            throw new SuppliesOutStock();
        } else if (ubication!=null && !ubication.validateUbication()) {
            throw new BusyLocation();
        }
        ConstructionRequest event = new ConstructionRequest(id, type,ubication);

        event.domainEvents.add(new ConstructionRequestCreatedDomainEvent(event.getId()));
        return event;
    }

    public void activate() {
        this.status = Status.PENDIENTE;
        // TODO: domain event
    }

    public void finalizado() {
        this.status = Status.FINALIZADO;

        domainEvents.add(new ConstructionRequestCancelledDomainEvent(id));
    }

    public void progreso() {
        this.status = Status.PROGRESO;
        // TODO: domain event
    }

    public void rescheduleDate(ConstructionRequestSchedule schedule) throws EndDateIsBeforeStartDate {
        this.schedule = schedule;

        domainEvents.add(new ConstructionRequestDateRescheduledDomainEvent(id));
    }
}
