package co.com.sura.ciudadela.infraestructure.rest.spring;

import co.com.sura.ciudadela.infraestructure.rest.Serializer.*;
import co.com.sura.ciudadela.model.event.ConstructionRequest;
import co.com.sura.ciudadela.model.event.ConstructionRequestId;
import org.springframework.boot.autoconfigure.jackson.Jackson2ObjectMapperBuilderCustomizer;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.core.annotation.Order;

@Configuration
public class SpringRestConfiguration {

    @Bean
    @Order(value = 10)
    public Jackson2ObjectMapperBuilderCustomizer jackson2ObjectMapperBuilderCustomizer() {
        return builder -> {
            builder.serializerByType(ConstructionRequest.class, new ConstructionRequestSerializer());
            builder.serializerByType(ConstructionRequestId.class, new ConstructionRequestIdSerializer());
            builder.serializerByType(ConstructionRequestDateSerializer.class, new ConstructionRequestDateSerializer());
            builder.serializerByType(ConstructionRequestScheduleSerializer.class, new ConstructionRequestScheduleSerializer());
            builder.serializerByType(ConstructionRequestTypeSerializer.class, new ConstructionRequestTypeSerializer());
            builder.serializerByType(ConstructionRequestLocationSerializer.class, new ConstructionRequestLocationSerializer());
        };
    }
}
