package co.com.sura.ciudadela.infraestructure;

import co.com.sura.ciudadela.model.event.ConstructionRequest;
import co.com.sura.ciudadela.model.event.ConstructionRequestId;
import co.com.sura.ciudadela.model.event.ConstructionRequestRepository;
import org.springframework.stereotype.Component;

import java.math.BigInteger;
import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.atomic.AtomicLong;

@Component
public class DataBaseConstructionRequestRepository implements ConstructionRequestRepository {
    private AtomicLong sequence;
    private Map<ConstructionRequestId, ConstructionRequest> events;

    public DataBaseConstructionRequestRepository() {
        this.sequence = new AtomicLong();
        this.events = new HashMap<>();
    }

    @Override
    public ConstructionRequestId getId() {
        Long id = sequence.addAndGet(1);
        return ConstructionRequestId.valueOf(new BigInteger(id.toString()));
    }

    @Override
    public ConstructionRequest findById(ConstructionRequestId id) {
        return events.get(id);
    }

    @Override
    public void persist(ConstructionRequest event) {
        events.put(event.getId(), event);
    }
}
