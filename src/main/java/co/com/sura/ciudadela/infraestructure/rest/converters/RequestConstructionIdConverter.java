package co.com.sura.ciudadela.infraestructure.rest.converters;

import co.com.sura.ciudadela.model.event.ConstructionRequestId;
import org.springframework.core.convert.converter.Converter;
import org.springframework.stereotype.Component;

@Component
public class RequestConstructionIdConverter implements Converter<String, ConstructionRequestId> {

    @Override
    public ConstructionRequestId convert(String source) {
        try {
            return ConstructionRequestId.valueOf(source);
        } catch (Exception e) {
            throw new IllegalArgumentException(e);
        }
    }
}
