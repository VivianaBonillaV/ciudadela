package co.com.sura.ciudadela.infraestructure.rest.Serializer;

import co.com.sura.ciudadela.model.event.ConstructionRequestType;
import com.fasterxml.jackson.core.JsonGenerator;
import com.fasterxml.jackson.databind.JsonSerializer;
import com.fasterxml.jackson.databind.SerializerProvider;
import org.springframework.boot.jackson.JsonComponent;

import java.io.IOException;

@JsonComponent
public class ConstructionRequestTypeSerializer extends JsonSerializer<ConstructionRequestType> {
    @Override
    public void serialize(ConstructionRequestType value, JsonGenerator generator, SerializerProvider serializers) throws IOException {
        generator.writeFieldName("type");
        serializers.defaultSerializeValue(value.getType(), generator);
    }
}
