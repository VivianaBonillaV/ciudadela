package co.com.sura.ciudadela.infraestructure.rest.converters;

import co.com.sura.ciudadela.model.event.ConstructionRequestDate;
import org.springframework.core.convert.converter.Converter;
import org.springframework.stereotype.Component;

@Component
public class RequestConstructionDateConverter implements Converter<String, ConstructionRequestDate> {
    @Override
    public ConstructionRequestDate convert(String source) {
        try {
            return ConstructionRequestDate.valueOf(source);
        } catch (Exception e) {
            throw new IllegalArgumentException(e);
        }
    }
}
