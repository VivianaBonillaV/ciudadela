package co.com.sura.ciudadela.infraestructure.rest.Serializer;

import co.com.sura.ciudadela.model.event.ConstructionRequestSchedule;
import com.fasterxml.jackson.core.JsonGenerator;
import com.fasterxml.jackson.databind.JsonSerializer;
import com.fasterxml.jackson.databind.SerializerProvider;
import org.springframework.boot.jackson.JsonComponent;

import java.io.IOException;

@JsonComponent
public class ConstructionRequestScheduleSerializer extends JsonSerializer<ConstructionRequestSchedule> {
    @Override
    public Class<ConstructionRequestSchedule> handledType() {
        return ConstructionRequestSchedule.class;
    }

    @Override
    public void serialize(ConstructionRequestSchedule value, JsonGenerator generator, SerializerProvider serializers) throws IOException {
        generator.writeStartObject();
        generator.writeFieldName("startDate");
        serializers.defaultSerializeValue(value.getStartDate(), generator);
        generator.writeFieldName("endDate");
        serializers.defaultSerializeValue(value.getEndDate(), generator);
        generator.writeEndObject();
    }
}
