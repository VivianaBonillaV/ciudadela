package co.com.sura.ciudadela.infraestructure.rest.controller;

import co.com.sura.ciudadela.application.command.CreateConstructionRequestCommand;
import co.com.sura.ciudadela.application.commandbus.CommandBus;
import co.com.sura.ciudadela.application.query.GetConstructionRequestQuery;
import co.com.sura.ciudadela.application.querybus.QueryBus;
import co.com.sura.ciudadela.model.event.*;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/solicitud/construccion")
public class ConstructionRequestController {
    private CommandBus commandBus;
    private QueryBus queryBus;

    public ConstructionRequestController(QueryBus queryBus, CommandBus commandBus) {
        this.queryBus = queryBus;
        this.commandBus = commandBus;
    }

    @PostMapping
    public ResponseEntity<String> createEvent(@RequestParam("tipoConstruccion") ConstructionRequestType typeBuild, @RequestParam("coordenadas") ConstructionRequestUbication ubication) throws Exception {
        CreateConstructionRequestCommand command = CreateConstructionRequestCommand.Builder.getInstance()
                .eventInformation(typeBuild,ubication)
                .build();
        commandBus.handle(command);
        return ResponseEntity.ok().build();
    }

    @GetMapping(path = "/{id}", produces = MediaType.APPLICATION_JSON_VALUE)
    @ResponseBody
    public ResponseEntity<ConstructionRequest> getEvent(@PathVariable("id") ConstructionRequestId constructionRequestId) throws Exception {
        GetConstructionRequestQuery command = GetConstructionRequestQuery.Builder.getInstance()
                .eventId(constructionRequestId)
                .build();
        ConstructionRequest constructionRequest = queryBus.handle(command);
        if (constructionRequest == null) {
            return ResponseEntity.notFound().build();
        }
        return ResponseEntity.ok().body(constructionRequest);
    }
}
