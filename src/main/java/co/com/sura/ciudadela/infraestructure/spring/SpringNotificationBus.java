package co.com.sura.ciudadela.infraestructure.spring;

import co.com.sura.ciudadela.model.domaineventbus.DomainEvent;
import co.com.sura.ciudadela.model.domaineventbus.DomainEventBus;
import org.springframework.context.annotation.Primary;
import org.springframework.stereotype.Component;

import java.time.format.DateTimeFormatter;

@Component
@Primary
public class SpringNotificationBus implements DomainEventBus {

    public void publish(DomainEvent event) {
        System.out.printf("%s %s %s%n", event.getClass().getName(), event.getId().getValue(), event.getDate().format(DateTimeFormatter.ISO_DATE_TIME));
    }
}
