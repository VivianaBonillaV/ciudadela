package co.com.sura.ciudadela.infraestructure.rest.Serializer;

import co.com.sura.ciudadela.model.event.ConstructionRequest;
import com.fasterxml.jackson.core.JsonGenerator;
import com.fasterxml.jackson.databind.JsonSerializer;
import com.fasterxml.jackson.databind.SerializerProvider;
import org.springframework.boot.jackson.JsonComponent;

import java.io.IOException;

@JsonComponent
public class ConstructionRequestSerializer extends JsonSerializer<ConstructionRequest> {

    @Override
    public Class<ConstructionRequest> handledType() {
        return ConstructionRequest.class;
    }

    @Override
    public void serialize(ConstructionRequest value, JsonGenerator generator, SerializerProvider serializers) throws IOException {
        generator.writeStartObject();
        generator.writeFieldName("id");
        serializers.defaultSerializeValue(value.getId(), generator);
        generator.writeFieldName("status");
        serializers.defaultSerializeValue(value.getStatus(), generator);
        generator.writeFieldName("schedule");
        serializers.defaultSerializeValue(value.getSchedule(), generator);
        generator.writeFieldName("type");
        serializers.defaultSerializeValue(value.getType(), generator);
        generator.writeFieldName("ubication");
        serializers.defaultSerializeValue(value.getUbication(), generator);
        generator.writeEndObject();
    }
}
