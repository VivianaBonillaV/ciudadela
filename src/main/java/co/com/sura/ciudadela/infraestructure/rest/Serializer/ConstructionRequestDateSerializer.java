package co.com.sura.ciudadela.infraestructure.rest.Serializer;

import co.com.sura.ciudadela.model.event.ConstructionRequestDate;
import com.fasterxml.jackson.core.JsonGenerator;
import com.fasterxml.jackson.databind.JsonSerializer;
import com.fasterxml.jackson.databind.SerializerProvider;
import org.springframework.boot.jackson.JsonComponent;

import java.io.IOException;
import java.time.format.DateTimeFormatter;

@JsonComponent
public class ConstructionRequestDateSerializer extends JsonSerializer<ConstructionRequestDate> {
    @Override
    public void serialize(ConstructionRequestDate value, JsonGenerator generator, SerializerProvider serializers) throws IOException {
        generator.writeString(value.geValue().format(DateTimeFormatter.ISO_DATE_TIME));
    }
}
