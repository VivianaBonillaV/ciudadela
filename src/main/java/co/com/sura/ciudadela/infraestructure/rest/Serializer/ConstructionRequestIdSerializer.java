package co.com.sura.ciudadela.infraestructure.rest.Serializer;

import co.com.sura.ciudadela.model.event.ConstructionRequestId;
import com.fasterxml.jackson.core.JsonGenerator;
import com.fasterxml.jackson.databind.JsonSerializer;
import com.fasterxml.jackson.databind.SerializerProvider;
import org.springframework.boot.jackson.JsonComponent;

import java.io.IOException;

@JsonComponent
public class ConstructionRequestIdSerializer extends JsonSerializer<ConstructionRequestId> {
    @Override
    public Class<ConstructionRequestId> handledType() {
        return ConstructionRequestId.class;
    }

    @Override
    public void serialize(ConstructionRequestId value, JsonGenerator generator, SerializerProvider serializers) throws IOException {
        generator.writeNumber(value.getValue());
    }

}
