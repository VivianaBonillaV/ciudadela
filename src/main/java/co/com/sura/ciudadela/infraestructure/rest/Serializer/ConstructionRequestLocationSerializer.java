package co.com.sura.ciudadela.infraestructure.rest.Serializer;

import co.com.sura.ciudadela.model.event.ConstructionRequestUbication;
import com.fasterxml.jackson.core.JsonGenerator;
import com.fasterxml.jackson.databind.JsonSerializer;
import com.fasterxml.jackson.databind.SerializerProvider;
import org.springframework.boot.jackson.JsonComponent;

import java.io.IOException;

@JsonComponent
public class ConstructionRequestLocationSerializer extends JsonSerializer<ConstructionRequestUbication> {
    @Override
    public void serialize(ConstructionRequestUbication value, JsonGenerator generator, SerializerProvider serializers) throws IOException {
        generator.writeString(value.getValue());
    }
}
